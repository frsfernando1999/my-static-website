# Pipeline Training

This project used a lot of CLI tools to create, test and deploy an static website. These tools were:

1. Docker with node installed
2. NPM for downloading dependencies/packages
3. Gatsby to generate a simple static website
4. Alpine for a few CLI Commands (ex.:grep)
5. Surge for deploying

## How it works

The pipeline is built on jobs (stages), each runs with a different purpose:

- Cache:
Runs on scheduled time, main purpose is to download NPM and save it so that other jobs can use it withouth having to download it every time they run.

- Build:
Creates the main project files.

- Test:
A smoke test is used to check if the the build was succesful, then, it normally runs the service/framework tests.

- Deploy Review:
Deploys the application on an reviewing environment, it pauses the pipeline so that people can check it, once you stop this job, it will go to Stop Review stage, which will tear down the application from its testing environment.

- Deploy Staging:
The final check before deploying it on a production environment.

- Deploy Production:
Deploys the application officially.

- Production Tests:
Checks if the application was succesfully deployed.


